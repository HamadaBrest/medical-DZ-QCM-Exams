import React, { Component } from 'react'
import { Image, Dimensions, StyleSheet } from 'react-native'
import { YellowBox } from 'react-native'
import {
  Body,
  Card,
  CardItem,
  Container,
  Content,
  DeckSwiper,
  Fab,
  Icon,
  Left,
  ListItem,
  Text,
  Thumbnail,
  View
} from 'native-base'
import { difference, map, filter } from 'lodash'
import HeaderButtons, {
  HeaderButton,
  Item
} from 'react-navigation-header-buttons'
import ProgressBarAnimated from 'react-native-progress-bar-animated'

import * as Utils from '../../utils'
import Dermatologie from '../../data/dermatologie'

const IoniconsHeaderButton = passMeFurther => (
  <HeaderButton {...passMeFurther} color="white" />
)

export default class QuestionsPage extends Component {
  constructor() {
    super()
    this.state = {
      cards: undefined,
      selectedAnswers: [],
      correctAnswers: [],
      answered: false,
      correct: false,
      currentQuestionIndex: 2,
      progress: 0
    }
  }

  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title'),
    headerStyle: {
      backgroundColor: '#70c3ce'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    },
    headerRight: (
      <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>
        <Item
          title={`${navigation.getParam('currentQuestionIndex', '0')}/${
            Object.keys(Dermatologie.qcm).length
          }`}
          buttonWrapperStyle={{
            backgroundColor: '#eb9e3e',
            borderRadius: 10
          }}
        />
      </HeaderButtons>
    )
  })

  handleSelectAnswer = (answerId, correctAnswers) => {
    if (this.state.correctAnswers.length === 0) {
      this.setState({ correctAnswers })
    }
    if (this.state.selectedAnswers.includes(answerId)) {
      this.setState({
        selectedAnswers: filter(
          this.state.selectedAnswers,
          answer => answer !== answerId
        )
      })
    } else {
      this.setState({
        selectedAnswers: [...this.state.selectedAnswers, answerId]
      })
    }
  }

  componentWillMount() {
    // TODO: randomize elements
    const cards = map(Dermatologie.qcm, (qst, uuid) => ({
      question: qst.question,
      choices: qst.choices,
      answers: qst.answers,
      image: require('../../imgs/question-icon.png')
    }))
    Utils.shuffleArray(cards)
    this.setState({ cards })
  }

  render() {
    const barWidth = Dimensions.get('screen').width
    const progressCustomStyles = {
      backgroundColor: '#5ca4da',
      borderRadius: 0,
      borderColor: 'white',
      height: 10
    }
    return this.props.navigation.getParam('title') === 'Dermatologie' ? (
      <Container>
        <ProgressBarAnimated
          width={barWidth}
          value={this.state.progress}
          backgroundColorOnComplete="#6CC644"
          {...progressCustomStyles}
        />
        <View style={{ marginTop: '10%' }}>
          <DeckSwiper
            ref={c => (this._deckSwiper = c)}
            dataSource={this.state.cards}
            renderEmpty={() => (
              <View style={{ alignSelf: 'center' }}>
                <Text>Over</Text>
              </View>
            )}
            onSwipeLeft={() => {
              this.setState({
                selectedAnswers: [],
                correctAnswers: [],
                answered: false,
                correct: false,
                currentQuestionIndex: this.state.currentQuestionIndex + 1
              })
              this.props.navigation.setParams({
                currentQuestionIndex: this.state.currentQuestionIndex
              })
            }}
            onSwipeRight={() => {
              this.setState({
                selectedAnswers: [],
                correctAnswers: [],
                answered: false,
                correct: false,
                currentQuestionIndex: this.state.currentQuestionIndex + 1
              })
              this.props.navigation.setParams({
                currentQuestionIndex: this.state.currentQuestionIndex
              })
            }}
            renderItem={item => (
              <Card style={{ elevation: 3 }}>
                <CardItem>
                  <Left>
                    <Thumbnail source={item.image} />
                    <Body>
                      <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
                        {item.question}
                      </Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                  <Content>
                    {map(item.choices, (choice, id) => (
                      <ListItem
                        key={`${choice}${id}`}
                        onPress={() =>
                          this.handleSelectAnswer(id, item.answers)
                        }
                        style={
                          this.state.answered
                            ? this.state.selectedAnswers.includes(id)
                              ? this.state.correctAnswers.includes(id)
                                ? { backgroundColor: '#53b66d' }
                                : { backgroundColor: '#e92824' }
                              : this.state.correctAnswers.includes(id) && {
                                  backgroundColor: '#53b66d'
                                }
                            : this.state.selectedAnswers.includes(id) && {
                                backgroundColor: '#70c3ce'
                              }
                        }
                      >
                        <Body>
                          <Text
                            style={
                              this.state.answered
                                ? this.state.selectedAnswers.includes(id)
                                  ? this.state.correctAnswers.includes(id)
                                    ? { color: 'white', fontSize: 14 }
                                    : { color: 'white', fontSize: 14 }
                                  : this.state.correctAnswers.includes(id) && {
                                      color: 'white',
                                      fontSize: 14
                                    }
                                : { fontSize: 14 }
                            }
                          >
                            {choice}
                          </Text>
                        </Body>
                      </ListItem>
                    ))}
                  </Content>
                </CardItem>
              </Card>
            )}
          />
        </View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={
            this.state.answered
              ? { backgroundColor: '#eb9e3e' }
              : { backgroundColor: '#5ca4da' }
          }
          position="bottomRight"
          onPress={() => {
            if (!this.state.answered) {
              const answer = difference(
                this.state.correctAnswers,
                this.state.selectedAnswers
              )
              if (answer.length === 0) {
                this.setState({ correct: true, answered: true })
              } else {
                this.setState({ correct: false, answered: true })
              }
            } else {
              this.setState({
                selectedAnswers: [],
                correctAnswers: [],
                answered: false,
                correct: false,
                currentQuestionIndex: this.state.currentQuestionIndex + 1,
                progress: (this.state.currentQuestionIndex * 100) / 40
              })
              this.props.navigation.setParams({
                currentQuestionIndex: this.state.currentQuestionIndex
              })
              this._deckSwiper._root.swipeRight()
            }
          }}
        >
          {this.state.answered ? (
            <Icon name="ios-arrow-forward" style={{ fontSize: 27 }} />
          ) : (
            <Icon name="ios-checkmark-outline" style={{ fontSize: 35 }} />
          )}
        </Fab>
      </Container>
    ) : (
      <Container>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Icon
            name="ios-construct"
            style={{ fontSize: 50, textAlign: 'center' }}
          />
          <Text
            style={{
              textAlign: 'center'
            }}
          >
            En construction
          </Text>
        </View>
      </Container>
    )
  }
}
