import PouchDB from 'pouchdb-react-native'
import React from 'react'
import { Text, Container, List, View, Content } from 'native-base'

import QuestionsPage from '../questions-page'

class ModulesPage extends React.Component {
  static navigationOptions = {
    title: 'Choisissez un module',
    headerStyle: {
      backgroundColor: '#70c3ce'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    }
  }

  constructor() {
    super()
  }

  render() {
    return (
      <Content>
        <List>{this.props.navigation.getParam('modules', [])}</List>
      </Content>
    )
  }
}

export default ModulesPage
