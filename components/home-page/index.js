import PouchDB from 'pouchdb-react-native'
import React from 'react'
import Modules from '../../data/modules'
import { Image } from 'react-native'
import { Container, Button, Text, List, ListItem } from 'native-base'
import { forEach, find } from 'lodash'
import { alphabet } from '../../utils'

class HomePage extends React.Component {
  static navigationOptions = {
    header: null
  }
  constructor() {
    super()
    this.state = {
      fontsLoaded: false,
      activeTab: 1,
      modules: undefined
    }
  }

  handlePressTab(activeTab) {
    this.setState({ activeTab })
  }

  async componentWillMount() {
    const modules = createModules(Modules, this.props)
    this.setState({ modules })
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('native-base/Fonts/Ionicons.ttf')
    })
    this.setState({ fontsLoaded: true })
  }

  render() {
    return this.state.fontsLoaded ? (
      <Container
        style={{
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Image
          source={require('../../imgs/med-student-icon.png')}
          style={{
            marginTop: '5%',
            marginBottom: '10%'
          }}
        />
        <Image
          source={require('../../imgs/title-logo.png')}
          style={{
            marginBottom: '20%',
            height: 50,
            width: 240
          }}
        />
        <Button
          block
          onPress={() =>
            this.props.navigation.navigate('Modules', {
              modules: this.state.modules
            })
          }
          light
          style={{ marginBottom: '5%' }}
        >
          <Text>Commencer un examen</Text>
        </Button>
        <Button block light style={{ marginBottom: '5%' }}>
          <Text>Vos notes</Text>
        </Button>
        <Button block light style={{ marginBottom: '5%' }}>
          <Text>Quitter</Text>
        </Button>
      </Container>
    ) : (
      <Text>Loading ...</Text>
    )
  }
}

export default HomePage

const createModules = (modules, { navigation }) => {
  let items = []
  forEach(alphabet, letter => {
    const moduleStartsWith = find(modules, (module, uuid) =>
      module.startsWith(letter)
    )
    if (moduleStartsWith) {
      items.push(
        <ListItem key={letter} itemDivider>
          <Text style={{ color: '#70c3ce' }}>{letter}</Text>
        </ListItem>
      )
      forEach(modules, (module, uuid) => {
        if (module.startsWith(letter)) {
          items.push(
            <ListItem
              key={uuid}
              onPress={() =>
                navigation.navigate('Questions', {
                  title: module,
                  currentQuestionIndex: 1
               })
              }
            >
              <Text>{module}</Text>
            </ListItem>
          )
        }
      })
    }
  })
  return items
}
