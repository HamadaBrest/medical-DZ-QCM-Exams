import React from 'react'
import { createStackNavigator } from 'react-navigation'

import HomePage from './components/home-page'
import ModulesPage from './components/modules-page'
import QuestionsPage from './components/questions-page'

export default createStackNavigator(
  {
    Home: HomePage,
    Modules: ModulesPage,
    Questions: QuestionsPage
  },
  {
    initialRouteName: 'Home'
  }
)
